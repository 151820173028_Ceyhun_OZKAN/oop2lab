﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace takeHome1
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            alphabetBox.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            radioButtonEnc.Checked = true;
            radioButtonCeaser.Checked = true;
        }


        public static bool IsAllDigits(string s)
        {
            foreach (char c in s)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
        public static bool IsAllLetters(string s)
        {
            foreach (char c in s)
            {
                if (!Char.IsLetter(c))
                    return false;
            }
            return true;
        }
        public static bool uniqueCharacters(String str)
        {
            for (int i = 0; i < str.Length; i++)
                for (int j = i + 1; j < str.Length; j++)
                    if (str[i] == str[j])
                        return false;
            return true;
        }

        public static string CeaserEncipher(string alphabet,string userstr, int rotvalue)
        {
            string output = String.Empty;
            int l = userstr.Length ,a =alphabet.Length, flag=0;
            if (IsAllDigits(rotvalue.ToString()) == false)
                MessageBox.Show("Enter integer values instead of rot value!", "WARNING!");
            if (uniqueCharacters(alphabet) == false)
                MessageBox.Show("Each letter of the alphabet must be unique", "WARNING!");
            if (IsAllLetters(alphabet) == false)
                MessageBox.Show("Enter Only Letters In The Alphabet Section!", "WARNING!");
            if (IsAllLetters(userstr) == false)
                MessageBox.Show("Enter Only Letters In The String Section!", "WARNING!");
            else
            {
                for (int i = 0; i < l; i++)
                {
                    for (int j = 0; j < a; j++)
                    {
                        if (userstr.ToUpper()[i] == alphabet.ToUpper()[j])  
                        {
                            output += alphabet[(j + rotvalue)%a];
                            flag = 1;
                        }
                    }
                    if (flag == 0)
                    {
                        MessageBox.Show("There is no such character in the alphabet!", "WARNING!");
                        return "";
                    }
                    flag = 0;
                }
            }
            return output;
        }

        public static string CeaserDecipher(string alphabet, string userstr, int rotvalue)
        {
            string output = String.Empty;
            int l = userstr.Length, a = alphabet.Length, flag = 0;
            if(IsAllDigits(rotvalue.ToString())==false)
                MessageBox.Show("Enter integer values instead of rot value!", "WARNING!");
            if (uniqueCharacters(alphabet) == false)
                MessageBox.Show("Each letter of the alphabet must be unique", "WARNING!");
            if (IsAllLetters(alphabet) == false)
                MessageBox.Show("Enter Only Letters In The Alphabet Section!", "WARNING!");
            if (IsAllLetters(userstr) == false)
                MessageBox.Show("Enter Only Letters In The String Section!", "WARNING!");
            else
            {
                for (int i = 0; i < l; i++)
                {
                    for (int j = 0; j < a; j++)   
                    {
                        if (userstr.ToUpper()[i] == alphabet.ToUpper()[j])  
                        {
                            
                            if(rotvalue>a && rotvalue%a>j)
                            {
                                if (rotvalue % a == 0)
                                    rotvalue = a;
                                else
                                    rotvalue = rotvalue % a;
                                output += alphabet[a+j-rotvalue];
                                flag = 1;
                            }
                            else if(rotvalue > a && rotvalue % a <= j)
                            {
                                if (rotvalue % a == 0)
                                    rotvalue = a;
                                else
                                    rotvalue = rotvalue % a;
                                output += alphabet[j - (rotvalue % a)];
                                flag = 1;
                            }
                            else if(rotvalue<=a && j<rotvalue)
                            {
                                output += alphabet[a + j - rotvalue];
                                flag = 1;
                            }
                            else if(rotvalue<=a && j>=rotvalue)
                            {
                                output += alphabet[j-(rotvalue%a)];
                                flag = 1;
                            }

                        }
                    }
                    if (flag == 0)
                    {
                        MessageBox.Show("There is no such character in the alphabet!", "WARNING!");
                        return "";
                    }
                    flag = 0;
                }
            }
            return output;
        }


        public static string VigenereEncipher(string alphabet, string userstr, string key)
        {
            string output = String.Empty;
            int l = userstr.Length, a = alphabet.Length, k = key.Length, flag = 0,lconv=0,r=0;
            int[] conv = new int[100];
            if (uniqueCharacters(alphabet) == false)
                MessageBox.Show("Each letter of the alphabet must be unique", "WARNING!");

            for (int i = 0; i < k;i++)
            {
                for(int j=0;j<a;j++)
                {
                    if(alphabet.ToUpper()[j] == key.ToUpper()[i])
                    {
                        conv[lconv] = j+1;
                        lconv++;
                        flag = 1;
                    }
                }
                if (flag == 0)
                {
                    MessageBox.Show("There is no such character in the alphabet!", "WARNING!");
                    return "";
                }
                flag = 0;
            }

            if (IsAllLetters(userstr) == false)
                MessageBox.Show("Enter Only Letters In The String Section!", "WARNING!");
            if (IsAllLetters(key) == false)
            {
                MessageBox.Show("Enter Only Letters In the Key Section!", "WARNING!");
            }
            if (IsAllLetters(alphabet) == false)
            {
                MessageBox.Show("Enter Only Letters In The Alphabet Section!", "WARNING!");
            }
            
            else
            {
                for (int i = 0; i < l; i++)
                {
                    for (int j = 0; j < a; j++)
                    {
                        if (userstr.ToUpper()[i] == alphabet.ToUpper()[j])  
                        {
  
                            output += alphabet[(j + conv[r]) % a];
                            if (r + 1 == lconv)
                               r = 0;
                            else
                               r++;

                        }
                    }
                    
                }
            }
            return output;
        }

        public static string VigenereDecipher(string alphabet, string userstr, string key)
        {
            string output = String.Empty;
            int l = userstr.Length, a = alphabet.Length, k = key.Length, flag = 0, lconv = 0, r = 0;
            int[] conv = new int[100];

            if (uniqueCharacters(alphabet) == false)
                MessageBox.Show("Each letter of the alphabet must be unique", "WARNING!");
            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < a; j++)
                {
                    if (alphabet.ToUpper()[j] == key.ToUpper()[i])
                    {
                        conv[lconv] = j + 1;
                        lconv++;
                        flag = 1;
                    }
                }
                if (flag == 0)
                {
                    MessageBox.Show("There is no such character in the alphabet!", "WARNING!");
                    return "";
                }
                flag = 0;
            }
            if (IsAllLetters(userstr) == false)
                MessageBox.Show("Enter Only Letters In String Section!", "WARNING!");
            if (IsAllLetters(key) == false)
            {
                MessageBox.Show("Enter Only Letters! In the Key Section!", "WARNING!");
            }
            if (IsAllLetters(alphabet) == false)
            {
                MessageBox.Show("Enter Only Letters In The Alphabet Section!", "WARNING!");
            }
            else
            {
                for (int i = 0; i < l; i++)
                {
                    for (int j = 0; j < a; j++)
                    {
                        if (userstr.ToUpper()[i] == alphabet.ToUpper()[j])  
                        {

                            if (conv[r] > a && conv[r] % a > j)
                            {
                                if (conv[r] % a == 0)
                                    conv[r] = a;
                                else
                                    conv[r] = conv[r] % a;
                                output += alphabet[a + j - conv[r]];

                                if (r + 1 == lconv)
                                    r = 0;
                                else
                                    r++;
                            }
                            else if (conv[r] > a && conv[r] % a <= j)
                            {
                                if (conv[r] % a == 0)
                                    conv[r] = a;
                                else
                                    conv[r] = conv[r] % a;
                                output += alphabet[j - (conv[r] % a)];
                                if (r + 1 == lconv)
                                    r = 0;
                                else
                                    r++;
                            }
                            else if (conv[r] <= a && j < conv[r])
                            {
                                output += alphabet[a + j - conv[r]];
                                if (r + 1 == lconv)
                                    r = 0;
                                else
                                    r++;

                            }
                            else if (conv[r] <= a && j >= conv[r])
                            {
                                output += alphabet[j - (conv[r] % a)];
                                if (r + 1 == lconv)
                                    r = 0;
                                else
                                    r++;

                            }
                        }
                    }
                    
                }
            }
            return output;
        }

        private void radioButtonCeaser_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButtonCeaser.Checked==true)
                lblKeyValue.Text = "ROT Value";
            else
                lblKeyValue.Text = "Key";

            if (alphabetBox.Text != "" && stringBox.Text != "" && keyBox.Text != "")
            {

                if (radioButtonCeaser.Checked == true && radioButtonEnc.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserEncipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };

                }
                if (radioButtonCeaser.Checked == true && radioButtonDec.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserDecipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };
                }


                if (radioButtonVigenere.Checked == true && radioButtonEnc.Checked == true)
                {
                    string cea = VigenereEncipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }
                if (radioButtonVigenere.Checked == true && radioButtonDec.Checked == true)
                {
                    string cea = VigenereDecipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }

            }
        }

        private void alphabetBox_TextChanged(object sender, EventArgs e)
        {
            if (alphabetBox.Text != "" && stringBox.Text != "" && keyBox.Text != "")
            {

                if (radioButtonCeaser.Checked == true && radioButtonEnc.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserEncipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };

                }
                if (radioButtonCeaser.Checked == true && radioButtonDec.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserDecipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };
                }


                if (radioButtonVigenere.Checked == true && radioButtonEnc.Checked == true)
                {
                    string cea = VigenereEncipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }
                if (radioButtonVigenere.Checked == true && radioButtonDec.Checked == true)
                {
                    string cea = VigenereDecipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }

            }


        }

        private void stringBox_TextChanged(object sender, EventArgs e)
        {
            if (alphabetBox.Text != "" && stringBox.Text != "" && keyBox.Text != "")
            {

                if (radioButtonCeaser.Checked == true && radioButtonEnc.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserEncipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };

                }
                if (radioButtonCeaser.Checked == true && radioButtonDec.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserDecipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };
                }


                if (radioButtonVigenere.Checked == true && radioButtonEnc.Checked == true)
                {
                    string cea = VigenereEncipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }
                if (radioButtonVigenere.Checked == true && radioButtonDec.Checked == true)
                {
                    string cea = VigenereDecipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }

            }
        }

        private void keyBox_TextChanged(object sender, EventArgs e)
        {
            if (alphabetBox.Text != "" && stringBox.Text != "" && keyBox.Text != "")
            {

                if (radioButtonCeaser.Checked == true && radioButtonEnc.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserEncipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };

                }
                if (radioButtonCeaser.Checked == true && radioButtonDec.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserDecipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                }
                    catch
                { MessageBox.Show("Please enter only integer values!", "WARNING!"); };
            }


                if (radioButtonVigenere.Checked == true && radioButtonEnc.Checked == true)
                {
                    string cea = VigenereEncipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }
                if (radioButtonVigenere.Checked == true && radioButtonDec.Checked == true)
                {
                    string cea = VigenereDecipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }

            }

        }

        private void radioButtonDec_CheckedChanged(object sender, EventArgs e)
        {
            if (alphabetBox.Text != "" && stringBox.Text != "" && keyBox.Text != "")
            {

                if (radioButtonCeaser.Checked == true && radioButtonEnc.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserEncipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };

                }
                if (radioButtonCeaser.Checked == true && radioButtonDec.Checked == true)
                {
                    try
                    {
                        string key = keyBox.Text;
                        int rotvalue = Convert.ToInt32(key);
                        string cea = CeaserDecipher(alphabetBox.Text, stringBox.Text, rotvalue);
                        resultLbl.Text = cea;
                    }
                    catch
                    { MessageBox.Show("Please enter only integer values!", "WARNING!"); };
                }


                if (radioButtonVigenere.Checked == true && radioButtonEnc.Checked == true)
                {
                    string cea = VigenereEncipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }
                if (radioButtonVigenere.Checked == true && radioButtonDec.Checked == true)
                {
                    string cea = VigenereDecipher(alphabetBox.Text, stringBox.Text, keyBox.Text);
                    resultLbl.Text = cea;
                }

            }
        }
    }
}
