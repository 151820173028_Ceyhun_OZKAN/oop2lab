﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace takeHome1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        double sayi1=0.0 , sayi2=0.0  , result=0.0;

        private void btnCikarma_Click(object sender, EventArgs e)
        {
            if (txtSayi1.Text == "" || txtSayi2.Text == "")
            {
                MessageBox.Show("Please enter 2 numbers", "WARNING!");
            }
            else if (IsDigitsOnly(txtSayi1.Text) && IsDigitsOnly(txtSayi2.Text))
            {
                sayi1 = Convert.ToDouble(txtSayi1.Text);
                sayi2 = Convert.ToDouble(txtSayi2.Text);
                lbSonuc.Text = Convert.ToString(sayi1 - sayi2);
                result = Convert.ToDouble(lbSonuc.Text);
                if (result > 0)
                    lbSonuc.BackColor = Color.Green;
                else
                    lbSonuc.BackColor = Color.Red;
                btnCikarma.BackColor = Color.MistyRose;
                btnToplama.BackColor = Color.BlanchedAlmond;
                btnBolme.BackColor = Color.BlanchedAlmond;
                btnCarpma.BackColor = Color.BlanchedAlmond;
            }
            else
                MessageBox.Show("Please only enter numbers!", "WARNING!");
        }

        private void btnCarpma_Click(object sender, EventArgs e)
        {
           
            if (txtSayi1.Text=="" || txtSayi2.Text == "")
            {
                MessageBox.Show("Please enter 2 numbers", "WARNING!");
            }
            else if (IsDigitsOnly(txtSayi1.Text) && IsDigitsOnly(txtSayi2.Text))
            {
                sayi1 = Convert.ToDouble(txtSayi1.Text);
                sayi2 = Convert.ToDouble(txtSayi2.Text);
                lbSonuc.Text = Convert.ToString(sayi1 * sayi2);
                result = Convert.ToDouble(lbSonuc.Text);
                if (result > 0)
                    lbSonuc.BackColor = Color.Green;
                else
                    lbSonuc.BackColor = Color.Red;
                btnCikarma.BackColor = Color.BlanchedAlmond;
                btnToplama.BackColor = Color.BlanchedAlmond;
                btnBolme.BackColor = Color.BlanchedAlmond;
                btnCarpma.BackColor = Color.MistyRose;
            }
            else
                MessageBox.Show("Please only enter numbers!", "WARNING!");
        }
        private void btnBolme_Click(object sender, EventArgs e)
        {
            if (txtSayi1.Text == "" || txtSayi2.Text == "")
            {
                MessageBox.Show("Please enter 2 numbers", "WARNING!");
            }
            else if (IsDigitsOnly(txtSayi1.Text) && IsDigitsOnly(txtSayi2.Text))
            {
                sayi1 = Convert.ToDouble(txtSayi1.Text);
                sayi2 = Convert.ToDouble(txtSayi2.Text);
                if (sayi1 == 0 && sayi2 == 0)
                {
                    lbSonuc.Text = "0/0 Error!";
                    lbSonuc.BackColor = Color.Black;
                }
                else if(sayi1 !=0 && sayi2==0)
                {
                    lbSonuc.Text = "Infinite ( ∞ )";
                    lbSonuc.BackColor = Color.HotPink;
                }
                else
                {
                    lbSonuc.Text = Convert.ToString(sayi1 / sayi2);
                    result = Convert.ToDouble(lbSonuc.Text);
                    if (result > 0)
                        lbSonuc.BackColor = Color.Green;
                    else
                        lbSonuc.BackColor = Color.Red;

                }
                btnCikarma.BackColor = Color.BlanchedAlmond;
                btnToplama.BackColor = Color.BlanchedAlmond;
                btnBolme.BackColor = Color.MistyRose;
                btnCarpma.BackColor = Color.BlanchedAlmond;
            }
            else
                MessageBox.Show("Please only enter numbers!", "WARNING!");
        }

        private void btnToplama_Click(object sender, EventArgs e)
        {
            if (txtSayi1.Text == "" || txtSayi2.Text == "")
            {
                MessageBox.Show("Please enter 2 numbers", "WARNING!");
            }
            else if (IsDigitsOnly(txtSayi1.Text) && IsDigitsOnly(txtSayi2.Text))
            {
                sayi1 = Convert.ToDouble(txtSayi1.Text);
                sayi2 = Convert.ToDouble(txtSayi2.Text);
                lbSonuc.Text = Convert.ToString(sayi1 + sayi2);
                result = Convert.ToDouble(lbSonuc.Text);
                if (result > 0)
                    lbSonuc.BackColor = Color.Green;
                else
                    lbSonuc.BackColor = Color.Red;
                btnCikarma.BackColor = Color.BlanchedAlmond;
                btnToplama.BackColor = Color.MistyRose;
                btnBolme.BackColor = Color.BlanchedAlmond;
                btnCarpma.BackColor = Color.BlanchedAlmond;
            }
            else
                MessageBox.Show("Please only enter numbers!", "WARNING!");
        }
        bool IsDigitsOnly(string str)
        {
            int dotCount = 0,commaCount=0,minusCount=0,plusCount=0,ct=0;
            int length = str.Length;
            foreach (char c in str)
            {
                if(c == '-')
                {
                    if (minusCount == 0 && str[0] == '-')
                        minusCount++;
                    else
                        return false;
                }
                else if(c == '+')
                {
                    if (plusCount == 0 && str[0] == '+')
                        plusCount++;
                    else
                        return false;
                }
                else if(c == '.')
                {
                    if(dotCount+commaCount==0 && str[length-1] !='.' && str[0]!='.' && str[ct-1]!='-' )
                        dotCount++;
                    else
                        return false;
                }
                else if(c == ',')
                {
                    if (dotCount + commaCount == 0 && str[length - 1] != ',' && str[0] != ',' && str[ct - 1] != '-')
                        commaCount++;
                    else
                        return false;
                }
                else if (c < '0' || c > '9')
                        return false;
                ct++;
            }

            return true;
        }
    }
}
