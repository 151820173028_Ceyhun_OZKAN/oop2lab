﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace takeHome1
{
    public class Users
    {
        private static Users instance;

        public Form1 f1;
        public Form2 f2;
        public Form3 f3;
        public Form4 f4;
        public Form5 f5;

        private Users() { }

        public static Users GetInstance()
        {
            if (instance == null)
                instance = new Users();
            return instance;
        }

        public Dictionary<string, string> account = new Dictionary<string, string>();

    }
}
