﻿
namespace takeHome1
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.lbKullaniciAdi = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.alphabetBox = new System.Windows.Forms.RichTextBox();
            this.stringBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.keyBox = new System.Windows.Forms.RichTextBox();
            this.lblKeyValue = new System.Windows.Forms.Label();
            this.resultLbl = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonDec = new System.Windows.Forms.RadioButton();
            this.radioButtonEnc = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButtonVigenere = new System.Windows.Forms.RadioButton();
            this.radioButtonCeaser = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // lbKullaniciAdi
            // 
            this.lbKullaniciAdi.AutoSize = true;
            this.lbKullaniciAdi.Location = new System.Drawing.Point(74, 6);
            this.lbKullaniciAdi.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbKullaniciAdi.Name = "lbKullaniciAdi";
            this.lbKullaniciAdi.Size = new System.Drawing.Size(0, 13);
            this.lbKullaniciAdi.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "User Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 49);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "Alphabet:";
            // 
            // alphabetBox
            // 
            this.alphabetBox.Location = new System.Drawing.Point(77, 46);
            this.alphabetBox.Margin = new System.Windows.Forms.Padding(2);
            this.alphabetBox.Name = "alphabetBox";
            this.alphabetBox.Size = new System.Drawing.Size(255, 24);
            this.alphabetBox.TabIndex = 18;
            this.alphabetBox.Text = "";
            this.alphabetBox.TextChanged += new System.EventHandler(this.alphabetBox_TextChanged);
            // 
            // stringBox
            // 
            this.stringBox.Location = new System.Drawing.Point(77, 98);
            this.stringBox.Margin = new System.Windows.Forms.Padding(2);
            this.stringBox.Name = "stringBox";
            this.stringBox.Size = new System.Drawing.Size(255, 24);
            this.stringBox.TabIndex = 20;
            this.stringBox.Text = "";
            this.stringBox.TextChanged += new System.EventHandler(this.stringBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 101);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "String:";
            // 
            // keyBox
            // 
            this.keyBox.Location = new System.Drawing.Point(77, 142);
            this.keyBox.Margin = new System.Windows.Forms.Padding(2);
            this.keyBox.Name = "keyBox";
            this.keyBox.Size = new System.Drawing.Size(255, 24);
            this.keyBox.TabIndex = 22;
            this.keyBox.Text = "";
            this.keyBox.TextChanged += new System.EventHandler(this.keyBox_TextChanged);
            // 
            // lblKeyValue
            // 
            this.lblKeyValue.AutoSize = true;
            this.lblKeyValue.Location = new System.Drawing.Point(11, 142);
            this.lblKeyValue.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblKeyValue.Name = "lblKeyValue";
            this.lblKeyValue.Size = new System.Drawing.Size(28, 13);
            this.lblKeyValue.TabIndex = 21;
            this.lblKeyValue.Text = "Key:";
            // 
            // resultLbl
            // 
            this.resultLbl.AutoSize = true;
            this.resultLbl.Location = new System.Drawing.Point(104, 189);
            this.resultLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.resultLbl.Name = "resultLbl";
            this.resultLbl.Size = new System.Drawing.Size(0, 13);
            this.resultLbl.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 189);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 23;
            this.label6.Text = "Result Text:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonDec);
            this.groupBox1.Controls.Add(this.radioButtonEnc);
            this.groupBox1.Location = new System.Drawing.Point(352, 255);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(193, 49);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cryption";
            // 
            // radioButtonDec
            // 
            this.radioButtonDec.AutoSize = true;
            this.radioButtonDec.Location = new System.Drawing.Point(97, 16);
            this.radioButtonDec.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonDec.Name = "radioButtonDec";
            this.radioButtonDec.Size = new System.Drawing.Size(76, 17);
            this.radioButtonDec.TabIndex = 1;
            this.radioButtonDec.TabStop = true;
            this.radioButtonDec.Text = "Decryption";
            this.radioButtonDec.UseVisualStyleBackColor = true;
            this.radioButtonDec.CheckedChanged += new System.EventHandler(this.radioButtonDec_CheckedChanged);
            // 
            // radioButtonEnc
            // 
            this.radioButtonEnc.AutoSize = true;
            this.radioButtonEnc.Location = new System.Drawing.Point(4, 16);
            this.radioButtonEnc.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonEnc.Name = "radioButtonEnc";
            this.radioButtonEnc.Size = new System.Drawing.Size(75, 17);
            this.radioButtonEnc.TabIndex = 0;
            this.radioButtonEnc.TabStop = true;
            this.radioButtonEnc.Text = "Encryption";
            this.radioButtonEnc.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButtonVigenere);
            this.groupBox2.Controls.Add(this.radioButtonCeaser);
            this.groupBox2.Location = new System.Drawing.Point(352, 308);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox2.Size = new System.Drawing.Size(193, 49);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cipher";
            // 
            // radioButtonVigenere
            // 
            this.radioButtonVigenere.AutoSize = true;
            this.radioButtonVigenere.Location = new System.Drawing.Point(97, 16);
            this.radioButtonVigenere.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonVigenere.Name = "radioButtonVigenere";
            this.radioButtonVigenere.Size = new System.Drawing.Size(67, 17);
            this.radioButtonVigenere.TabIndex = 1;
            this.radioButtonVigenere.TabStop = true;
            this.radioButtonVigenere.Text = "Vigenère";
            this.radioButtonVigenere.UseVisualStyleBackColor = true;
            // 
            // radioButtonCeaser
            // 
            this.radioButtonCeaser.AutoSize = true;
            this.radioButtonCeaser.Location = new System.Drawing.Point(4, 16);
            this.radioButtonCeaser.Margin = new System.Windows.Forms.Padding(2);
            this.radioButtonCeaser.Name = "radioButtonCeaser";
            this.radioButtonCeaser.Size = new System.Drawing.Size(58, 17);
            this.radioButtonCeaser.TabIndex = 0;
            this.radioButtonCeaser.TabStop = true;
            this.radioButtonCeaser.Text = "Ceaser";
            this.radioButtonCeaser.UseVisualStyleBackColor = true;
            this.radioButtonCeaser.CheckedChanged += new System.EventHandler(this.radioButtonCeaser_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::takeHome1.Properties.Resources.user_male;
            this.pictureBox1.Location = new System.Drawing.Point(340, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(210, 231);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(28, 234);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(122, 123);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(180, 234);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(133, 123);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 31;
            this.pictureBox3.TabStop = false;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 389);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.resultLbl);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.keyBox);
            this.Controls.Add(this.lblKeyValue);
            this.Controls.Add(this.stringBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.alphabetBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbKullaniciAdi);
            this.Controls.Add(this.label4);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cipher";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbKullaniciAdi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox alphabetBox;
        private System.Windows.Forms.RichTextBox stringBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox keyBox;
        private System.Windows.Forms.Label lblKeyValue;
        public System.Windows.Forms.Label resultLbl;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonDec;
        private System.Windows.Forms.RadioButton radioButtonEnc;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButtonVigenere;
        private System.Windows.Forms.RadioButton radioButtonCeaser;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}