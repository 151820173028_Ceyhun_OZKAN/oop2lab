﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace takeHome1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        private void calculatorBtn_Click(object sender, EventArgs e)
        {
            Program.user.f2 = new Form2();
            Program.user.f2.Show();
            Program.user.f2.lbKullaniciAdi2.Text = this.lbKullaniciAdi.Text;
            this.Hide();
        }
        private void cipherBtn_Click(object sender, EventArgs e)
        {
            Program.user.f4 = new Form4();
            Program.user.f4.Show();
            Program.user.f4.lbKullaniciAdi.Text = this.lbKullaniciAdi.Text;
            this.Hide();
        }
        private void btnGuessed_Click(object sender, EventArgs e)
        {
            Program.user.f5 = new Form5();
            Program.user.f5.Show();
            Program.user.f5.lbKullaniciAdi.Text = this.lbKullaniciAdi.Text;
            this.Hide();
        }
        private void Form3_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        
    }
}
