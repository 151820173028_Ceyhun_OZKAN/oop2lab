﻿
namespace takeHome1
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbKullaniciAdi = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.calculatorBtn = new System.Windows.Forms.Button();
            this.cipherBtn = new System.Windows.Forms.Button();
            this.btnGuessed = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbKullaniciAdi
            // 
            this.lbKullaniciAdi.AutoSize = true;
            this.lbKullaniciAdi.Location = new System.Drawing.Point(74, 6);
            this.lbKullaniciAdi.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbKullaniciAdi.Name = "lbKullaniciAdi";
            this.lbKullaniciAdi.Size = new System.Drawing.Size(0, 13);
            this.lbKullaniciAdi.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 6);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "User Name:";
            // 
            // calculatorBtn
            // 
            this.calculatorBtn.Location = new System.Drawing.Point(11, 31);
            this.calculatorBtn.Margin = new System.Windows.Forms.Padding(2);
            this.calculatorBtn.Name = "calculatorBtn";
            this.calculatorBtn.Size = new System.Drawing.Size(128, 42);
            this.calculatorBtn.TabIndex = 15;
            this.calculatorBtn.Text = "Calculator";
            this.calculatorBtn.UseVisualStyleBackColor = true;
            this.calculatorBtn.Click += new System.EventHandler(this.calculatorBtn_Click);
            // 
            // cipherBtn
            // 
            this.cipherBtn.Location = new System.Drawing.Point(161, 31);
            this.cipherBtn.Margin = new System.Windows.Forms.Padding(2);
            this.cipherBtn.Name = "cipherBtn";
            this.cipherBtn.Size = new System.Drawing.Size(128, 42);
            this.cipherBtn.TabIndex = 16;
            this.cipherBtn.Text = "Cipher";
            this.cipherBtn.UseVisualStyleBackColor = true;
            this.cipherBtn.Click += new System.EventHandler(this.cipherBtn_Click);
            // 
            // btnGuessed
            // 
            this.btnGuessed.Location = new System.Drawing.Point(313, 31);
            this.btnGuessed.Margin = new System.Windows.Forms.Padding(2);
            this.btnGuessed.Name = "btnGuessed";
            this.btnGuessed.Size = new System.Drawing.Size(128, 42);
            this.btnGuessed.TabIndex = 17;
            this.btnGuessed.Text = "Guessed Coordinate";
            this.btnGuessed.UseVisualStyleBackColor = true;
            this.btnGuessed.Click += new System.EventHandler(this.btnGuessed_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 292);
            this.Controls.Add(this.btnGuessed);
            this.Controls.Add(this.cipherBtn);
            this.Controls.Add(this.calculatorBtn);
            this.Controls.Add(this.lbKullaniciAdi);
            this.Controls.Add(this.label4);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Applications";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form3_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label lbKullaniciAdi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button calculatorBtn;
        private System.Windows.Forms.Button cipherBtn;
        private System.Windows.Forms.Button btnGuessed;
    }
}