﻿
namespace takeHome1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.txtSayi1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSayi2 = new System.Windows.Forms.TextBox();
            this.btnToplama = new System.Windows.Forms.Button();
            this.btnCarpma = new System.Windows.Forms.Button();
            this.btnCikarma = new System.Windows.Forms.Button();
            this.btnBolme = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.lbSonuc = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbKullaniciAdi2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txtSayi1
            // 
            this.txtSayi1.Location = new System.Drawing.Point(235, 93);
            this.txtSayi1.Margin = new System.Windows.Forms.Padding(5);
            this.txtSayi1.Name = "txtSayi1";
            this.txtSayi1.Size = new System.Drawing.Size(172, 29);
            this.txtSayi1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Number:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Second Number:";
            // 
            // txtSayi2
            // 
            this.txtSayi2.Location = new System.Drawing.Point(235, 148);
            this.txtSayi2.Margin = new System.Windows.Forms.Padding(5);
            this.txtSayi2.Name = "txtSayi2";
            this.txtSayi2.Size = new System.Drawing.Size(172, 29);
            this.txtSayi2.TabIndex = 3;
            // 
            // btnToplama
            // 
            this.btnToplama.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.btnToplama.Location = new System.Drawing.Point(39, 214);
            this.btnToplama.Name = "btnToplama";
            this.btnToplama.Size = new System.Drawing.Size(73, 65);
            this.btnToplama.TabIndex = 4;
            this.btnToplama.Text = "+";
            this.btnToplama.UseVisualStyleBackColor = false;
            this.btnToplama.Click += new System.EventHandler(this.btnToplama_Click);
            // 
            // btnCarpma
            // 
            this.btnCarpma.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.btnCarpma.Location = new System.Drawing.Point(235, 214);
            this.btnCarpma.Name = "btnCarpma";
            this.btnCarpma.Size = new System.Drawing.Size(73, 65);
            this.btnCarpma.TabIndex = 5;
            this.btnCarpma.Text = "*";
            this.btnCarpma.UseVisualStyleBackColor = false;
            this.btnCarpma.Click += new System.EventHandler(this.btnCarpma_Click);
            // 
            // btnCikarma
            // 
            this.btnCikarma.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.btnCikarma.ForeColor = System.Drawing.Color.Black;
            this.btnCikarma.Location = new System.Drawing.Point(138, 214);
            this.btnCikarma.Name = "btnCikarma";
            this.btnCikarma.Size = new System.Drawing.Size(73, 65);
            this.btnCikarma.TabIndex = 6;
            this.btnCikarma.Text = "-";
            this.btnCikarma.UseVisualStyleBackColor = false;
            this.btnCikarma.Click += new System.EventHandler(this.btnCikarma_Click);
            // 
            // btnBolme
            // 
            this.btnBolme.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.btnBolme.Location = new System.Drawing.Point(334, 214);
            this.btnBolme.Name = "btnBolme";
            this.btnBolme.Size = new System.Drawing.Size(73, 65);
            this.btnBolme.TabIndex = 7;
            this.btnBolme.Text = "/";
            this.btnBolme.UseVisualStyleBackColor = false;
            this.btnBolme.Click += new System.EventHandler(this.btnBolme_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 309);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 24);
            this.label3.TabIndex = 8;
            this.label3.Text = "Result:";
            // 
            // lbSonuc
            // 
            this.lbSonuc.AutoSize = true;
            this.lbSonuc.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lbSonuc.Location = new System.Drawing.Point(245, 309);
            this.lbSonuc.Name = "lbSonuc";
            this.lbSonuc.Size = new System.Drawing.Size(140, 24);
            this.lbSonuc.TabIndex = 9;
            this.lbSonuc.Text = "_____________";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::takeHome1.Properties.Resources.user_male;
            this.pictureBox1.Location = new System.Drawing.Point(453, 50);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(228, 247);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 33);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 24);
            this.label4.TabIndex = 11;
            this.label4.Text = "User Name:";
            // 
            // lbKullaniciAdi2
            // 
            this.lbKullaniciAdi2.AutoSize = true;
            this.lbKullaniciAdi2.Location = new System.Drawing.Point(231, 33);
            this.lbKullaniciAdi2.Name = "lbKullaniciAdi2";
            this.lbKullaniciAdi2.Size = new System.Drawing.Size(0, 24);
            this.lbKullaniciAdi2.TabIndex = 12;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 351);
            this.Controls.Add(this.lbKullaniciAdi2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbSonuc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnBolme);
            this.Controls.Add(this.btnCikarma);
            this.Controls.Add(this.btnCarpma);
            this.Controls.Add(this.btnToplama);
            this.Controls.Add(this.txtSayi2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSayi1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Windows Calculator";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtSayi1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSayi2;
        private System.Windows.Forms.Button btnToplama;
        private System.Windows.Forms.Button btnCarpma;
        private System.Windows.Forms.Button btnCikarma;
        private System.Windows.Forms.Button btnBolme;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbSonuc;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lbKullaniciAdi2;
    }
}