﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace takeHome1
{
    public partial class Form5 : Form
    {
        private int minX = 0;
        private int minY = 0;
        private int maxX = 1023;
        private int maxY = 1023;

        private GuessedCoordinate coordinate;

        private Random random = new Random();
        public Form5()
        {
            InitializeComponent();
            coordinate = GuessCoordinate(1, -1);
        }
        private GuessedCoordinate GuessCoordinate(int i, int j)
        {
            GuessedCoordinate _coordinate = new GuessedCoordinate();
            _coordinate.X = Convert.ToBoolean(Math.Abs(i)) ? random.Next(minX, maxX) : coordinate.X;
            _coordinate.Y = Convert.ToBoolean(Math.Abs(j)) ? random.Next(minY, maxY) : coordinate.Y;

            lblCoordinate.Text = "X: " + _coordinate.X.ToString() + " Y: " + _coordinate.Y.ToString();

            return _coordinate;
        }
        /**
        *   @param i indicates directory of the button
        *   @param j indicates directory of the button
        */
        private void DivideArea(int i, int j)
        {
            switch (i)
            {
                case 1:
                    {
                        minX = coordinate.X;
                        break;
                    }
                case -1:
                    {
                        maxX = coordinate.X;
                        break;
                    }
                default: //0
                    break;
            }
            switch (j)
            {
                case 1:
                    {
                        maxY = coordinate.Y;
                        break;
                    }
                case -1:
                    {
                        minY = coordinate.Y;
                        break;
                    }
                default: //0
                    break;
            }
        }

        private void btnNorthEast_Click(object sender, EventArgs e)
        {
            DivideArea(1, 1);
            coordinate = GuessCoordinate(1, 1);
        }

        private void btnEast_Click(object sender, EventArgs e)
        {
            DivideArea(1, 0);
            coordinate = GuessCoordinate(1, 0);
        }

        private void btnSouthEast_Click(object sender, EventArgs e)
        {
            DivideArea(1, -1);
            coordinate = GuessCoordinate(1, -1);
        }

        private void btnSouth_Click(object sender, EventArgs e)
        {
            DivideArea(0, -1);
            coordinate = GuessCoordinate(0, -1);
        }
        private void btnNorth_Click(object sender, EventArgs e)
        {
            DivideArea(0, 1);
            coordinate = GuessCoordinate(0, 1);
        }
        private void btnSouthWest_Click(object sender, EventArgs e)
        {
            DivideArea(-1, -1);
            coordinate = GuessCoordinate(-1, -1);
        }

        private void btnWest_Click(object sender, EventArgs e)
        {
            DivideArea(-1, 0);
            coordinate = GuessCoordinate(-1, 0);
        }

        private void btnNorthWest_Click(object sender, EventArgs e)
        {
            DivideArea(-1, 1);
            coordinate = GuessCoordinate(-1, 1);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            minX = 0;
            minY = 0;
            maxX = 1023;
            maxY = 1023;

            coordinate = GuessCoordinate(1, -1);
        }

        private void Form5_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
