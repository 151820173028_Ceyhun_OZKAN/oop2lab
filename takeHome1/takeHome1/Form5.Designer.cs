﻿
namespace takeHome1
{
    partial class Form5
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNorthWest = new System.Windows.Forms.Button();
            this.btnNorth = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.btnWest = new System.Windows.Forms.Button();
            this.btnSouth = new System.Windows.Forms.Button();
            this.btnSouthWest = new System.Windows.Forms.Button();
            this.btnEast = new System.Windows.Forms.Button();
            this.btnNorthEast = new System.Windows.Forms.Button();
            this.btnSouthEast = new System.Windows.Forms.Button();
            this.lblGuessedCoordinate = new System.Windows.Forms.Label();
            this.lblCoordinate = new System.Windows.Forms.Label();
            this.lbKullaniciAdi = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNorthWest
            // 
            this.btnNorthWest.Location = new System.Drawing.Point(4, 7);
            this.btnNorthWest.Margin = new System.Windows.Forms.Padding(4);
            this.btnNorthWest.Name = "btnNorthWest";
            this.btnNorthWest.Size = new System.Drawing.Size(112, 81);
            this.btnNorthWest.TabIndex = 0;
            this.btnNorthWest.Text = "NorthWest";
            this.btnNorthWest.UseVisualStyleBackColor = true;
            this.btnNorthWest.Click += new System.EventHandler(this.btnNorthWest_Click);
            // 
            // btnNorth
            // 
            this.btnNorth.Location = new System.Drawing.Point(124, 7);
            this.btnNorth.Margin = new System.Windows.Forms.Padding(4);
            this.btnNorth.Name = "btnNorth";
            this.btnNorth.Size = new System.Drawing.Size(112, 81);
            this.btnNorth.TabIndex = 1;
            this.btnNorth.Text = "North";
            this.btnNorth.UseVisualStyleBackColor = true;
            this.btnNorth.Click += new System.EventHandler(this.btnNorth_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(120, 96);
            this.btnReset.Margin = new System.Windows.Forms.Padding(4);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(112, 81);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Reset";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnWest
            // 
            this.btnWest.Location = new System.Drawing.Point(4, 96);
            this.btnWest.Margin = new System.Windows.Forms.Padding(4);
            this.btnWest.Name = "btnWest";
            this.btnWest.Size = new System.Drawing.Size(112, 81);
            this.btnWest.TabIndex = 2;
            this.btnWest.Text = "West";
            this.btnWest.UseVisualStyleBackColor = true;
            this.btnWest.Click += new System.EventHandler(this.btnWest_Click);
            // 
            // btnSouth
            // 
            this.btnSouth.Location = new System.Drawing.Point(120, 185);
            this.btnSouth.Margin = new System.Windows.Forms.Padding(4);
            this.btnSouth.Name = "btnSouth";
            this.btnSouth.Size = new System.Drawing.Size(112, 81);
            this.btnSouth.TabIndex = 7;
            this.btnSouth.Text = "South";
            this.btnSouth.UseVisualStyleBackColor = true;
            this.btnSouth.Click += new System.EventHandler(this.btnSouth_Click);
            // 
            // btnSouthWest
            // 
            this.btnSouthWest.Location = new System.Drawing.Point(4, 185);
            this.btnSouthWest.Margin = new System.Windows.Forms.Padding(4);
            this.btnSouthWest.Name = "btnSouthWest";
            this.btnSouthWest.Size = new System.Drawing.Size(112, 81);
            this.btnSouthWest.TabIndex = 6;
            this.btnSouthWest.Text = "South West";
            this.btnSouthWest.UseVisualStyleBackColor = true;
            this.btnSouthWest.Click += new System.EventHandler(this.btnSouthWest_Click);
            // 
            // btnEast
            // 
            this.btnEast.Location = new System.Drawing.Point(240, 96);
            this.btnEast.Margin = new System.Windows.Forms.Padding(4);
            this.btnEast.Name = "btnEast";
            this.btnEast.Size = new System.Drawing.Size(112, 81);
            this.btnEast.TabIndex = 5;
            this.btnEast.Text = "East";
            this.btnEast.UseVisualStyleBackColor = true;
            this.btnEast.Click += new System.EventHandler(this.btnEast_Click);
            // 
            // btnNorthEast
            // 
            this.btnNorthEast.Location = new System.Drawing.Point(240, 7);
            this.btnNorthEast.Margin = new System.Windows.Forms.Padding(4);
            this.btnNorthEast.Name = "btnNorthEast";
            this.btnNorthEast.Size = new System.Drawing.Size(112, 81);
            this.btnNorthEast.TabIndex = 4;
            this.btnNorthEast.Text = "North West";
            this.btnNorthEast.UseVisualStyleBackColor = true;
            this.btnNorthEast.Click += new System.EventHandler(this.btnNorthEast_Click);
            // 
            // btnSouthEast
            // 
            this.btnSouthEast.Location = new System.Drawing.Point(240, 185);
            this.btnSouthEast.Margin = new System.Windows.Forms.Padding(4);
            this.btnSouthEast.Name = "btnSouthEast";
            this.btnSouthEast.Size = new System.Drawing.Size(112, 81);
            this.btnSouthEast.TabIndex = 8;
            this.btnSouthEast.Text = "South East";
            this.btnSouthEast.UseVisualStyleBackColor = true;
            this.btnSouthEast.Click += new System.EventHandler(this.btnSouthEast_Click);
            // 
            // lblGuessedCoordinate
            // 
            this.lblGuessedCoordinate.AutoSize = true;
            this.lblGuessedCoordinate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblGuessedCoordinate.Location = new System.Drawing.Point(21, 88);
            this.lblGuessedCoordinate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblGuessedCoordinate.Name = "lblGuessedCoordinate";
            this.lblGuessedCoordinate.Size = new System.Drawing.Size(271, 29);
            this.lblGuessedCoordinate.TabIndex = 9;
            this.lblGuessedCoordinate.Text = "Guessed Coordinates: ";
            // 
            // lblCoordinate
            // 
            this.lblCoordinate.AutoSize = true;
            this.lblCoordinate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblCoordinate.Location = new System.Drawing.Point(300, 88);
            this.lblCoordinate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCoordinate.Name = "lblCoordinate";
            this.lblCoordinate.Size = new System.Drawing.Size(0, 29);
            this.lblCoordinate.TabIndex = 10;
            // 
            // lbKullaniciAdi
            // 
            this.lbKullaniciAdi.AutoSize = true;
            this.lbKullaniciAdi.Location = new System.Drawing.Point(112, 9);
            this.lbKullaniciAdi.Name = "lbKullaniciAdi";
            this.lbKullaniciAdi.Size = new System.Drawing.Size(0, 17);
            this.lbKullaniciAdi.TabIndex = 18;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "User Name:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnNorthWest);
            this.panel1.Controls.Add(this.btnNorth);
            this.panel1.Controls.Add(this.btnWest);
            this.panel1.Controls.Add(this.btnReset);
            this.panel1.Controls.Add(this.btnNorthEast);
            this.panel1.Controls.Add(this.btnSouthEast);
            this.panel1.Controls.Add(this.btnEast);
            this.panel1.Controls.Add(this.btnSouth);
            this.panel1.Controls.Add(this.btnSouthWest);
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.panel1.Location = new System.Drawing.Point(26, 153);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 273);
            this.panel1.TabIndex = 19;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::takeHome1.Properties.Resources.Compass_icon1;
            this.pictureBox1.Location = new System.Drawing.Point(402, 153);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(297, 273);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 20;
            this.pictureBox1.TabStop = false;
            // 
            // Form5
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 459);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lbKullaniciAdi);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblCoordinate);
            this.Controls.Add(this.lblGuessedCoordinate);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form5";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Guessed Coordinates";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form5_FormClosing);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNorthWest;
        private System.Windows.Forms.Button btnNorth;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Button btnWest;
        private System.Windows.Forms.Button btnSouth;
        private System.Windows.Forms.Button btnSouthWest;
        private System.Windows.Forms.Button btnEast;
        private System.Windows.Forms.Button btnNorthEast;
        private System.Windows.Forms.Button btnSouthEast;
        private System.Windows.Forms.Label lblGuessedCoordinate;
        private System.Windows.Forms.Label lblCoordinate;
        public System.Windows.Forms.Label lbKullaniciAdi;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}