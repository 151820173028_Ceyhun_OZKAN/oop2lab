﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace takeHome1
{
    static class Program
    {
        public static Users user = Users.GetInstance();
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            user.f1 = new Form1();
            Application.Run(user.f1);
        }
    }
}
