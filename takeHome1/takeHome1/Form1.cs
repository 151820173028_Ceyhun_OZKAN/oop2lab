﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography;


namespace takeHome1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Program.user.f3 = new Form3();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer.Interval = 1000;
            lbConfirmPassword.Visible = false;
            txtConfirmPassword.Visible = false;
            btnRegisterIn.Visible = false;
        }

        int sayac = 0;
       
        Users user = Users.GetInstance(); //singleton
      
        private void btnGirisYap_Click(object sender, EventArgs e)
        {
            if (this.txtUserName.Text == "" && this.txtPassword.Text == "")
                MessageBox.Show("Username and password are required!", "WARNING!");
            else if (this.txtUserName.Text == "" && this.txtPassword.Text != "")
                MessageBox.Show("Username is required!", "WARNING!");
            else if (this.txtUserName.Text != "" && this.txtPassword.Text == "")
                MessageBox.Show("Password is required!", "WARNING!");
            else
            {
                string sifrelenmisPassword = txtPassword.Text;
                sifrelenmisPassword = Encrypt(txtPassword.Text);
                bool ctrl = false;
                foreach (var kvp in user.account)
                {
                    if (kvp.Key == this.txtUserName.Text && kvp.Value == sifrelenmisPassword)
                    {
                        ctrl = true;
                        break;
                    }
                }

                if (ctrl)
                {
                    txtKontrol.Text = "Login successfully.";
                    txtKontrol.ForeColor = Color.Green;
                    timer.Start();
                }
                else
                {
                    txtKontrol.Text = "Login failed.";
                    txtKontrol.ForeColor = Color.Red;
                }

                Program.user.f3.lbKullaniciAdi.Text = txtUserName.Text;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
           Application.Exit();
        }
        private void timer_Tick(object sender, EventArgs e)
        {
            sayac++;
            if (sayac == 3)
            {
                sayac = 0;
                Program.user.f3.Show();
                this.Hide();
                timer.Stop();
            }            
            Console.WriteLine(sayac);
        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            btnGirisYap.Visible = true;
            lbConfirmPassword.Visible = true;
            txtConfirmPassword.Visible = true;
            btnRegister.Hide();
            label4.Hide();
            txtKontrol.Hide();
            btnGirisYap.Hide();
            btnRegisterIn.Show();
            label3.Text = "Register Screen";
            
        }

        private void btnRegisterIn_MouseClick(object sender, MouseEventArgs e)
        {
            bool ctrl = true, validation = false;
            if (user.account != null)
            {
                foreach (var kvp in user.account)
                    if (kvp.Key == this.txtUserName.Text)
                    {
                        ctrl = false;
                        break;
                    }
            }
            string sifrelenmisPassword = txtPassword.Text;
            sifrelenmisPassword = Encrypt(txtPassword.Text);
            //Console.WriteLine("Password: " +sifrelenmisPassword);
            
            string sifrelenmisConfirmPassword = txtConfirmPassword.Text;
            sifrelenmisConfirmPassword = Encrypt(txtConfirmPassword.Text);
           // Console.WriteLine("Confirm Password: " + sifrelenmisConfirmPassword);
            
            if (ctrl)
            {
                if (this.txtUserName.Text == "" && txtPassword.Text == "" && txtConfirmPassword.Text == "")
                    MessageBox.Show("Username, password and confirm password are required!", "WARNING!");
                else if (this.txtUserName.Text == "" && txtPassword.Text != "" && txtConfirmPassword.Text != "")
                    MessageBox.Show("Username is required!", "WARNING!");
                else if (this.txtUserName.Text == "" && txtPassword.Text != "" && txtConfirmPassword.Text == "")
                    MessageBox.Show("Username and confirm password are required!", "WARNING!");
                else if (this.txtUserName.Text != "" && txtPassword.Text == "" && txtConfirmPassword.Text != "")
                    MessageBox.Show("Password is required!", "WARNING!");
                else if (this.txtUserName.Text != "" && txtPassword.Text != "" && txtConfirmPassword.Text == "")
                    MessageBox.Show("Confirm password is required!", "WARNING!");
                else if (this.txtUserName.Text != "" && txtPassword.Text == "" && txtConfirmPassword.Text == "")
                    MessageBox.Show("Password and confirm password are required!", "WARNING!");
                else if (this.txtUserName.Text == "" && txtPassword.Text == "" && txtConfirmPassword.Text != "")
                    MessageBox.Show("Username and password are required!", "WARNING!");
                else if (sifrelenmisConfirmPassword != sifrelenmisPassword)
                    MessageBox.Show("Passwords do not match!", "WARNING!");
                else
                {
                    user.account.Add(this.txtUserName.Text, sifrelenmisPassword);
                    MessageBox.Show("Successful!", "DONE");
                    validation = true;
                }
            }
            else
                MessageBox.Show("This username is already taken!", "WARNING!");
            if (validation)
            {
                btnRegisterIn.Hide();
                txtConfirmPassword.Hide();
                lbConfirmPassword.Hide();
                btnRegister.Show();
                label4.Show();
                txtKontrol.Show();
                btnGirisYap.Show();
                label3.Text = "Calculator Login Screen";
            }
        }

        string Encrypt(string text)
        {
          
            // Creates an instance of the default implementation of the MD5 hash algorithm.
            using (var md5Hash = MD5.Create())
            {
                // Byte array representation of source string
                var sourceBytes = Encoding.UTF8.GetBytes(text);

                // Generate hash value(Byte Array) for input data
                var hashBytes = md5Hash.ComputeHash(sourceBytes);

                // Convert hash byte array to string
                var hash = BitConverter.ToString(hashBytes).Replace("-", string.Empty);

                // Output the MD5 hash
                return(hash);
            }
        }
    }       
}
